<x-guest-layout>
	<div class="flex flex-col md:flex-row items-center">
		<div class="w-full md:w-1/2 h-auto md:h-screen bg-white flex items-center justify-center px-0 py-20 md:py-10 md:px-10">
			<div class="w-4/12">
				<img src="{{ asset('/img/logo_csipi.jpg') }}" class="w-full h-auto" alt="">
			</div>
		</div>
		<div class="w-full md:w-1/2 h-auto md:h-screen px-10 py-10 md:px-16 md:py-16 flex flex-col items-start justify-center" style="background-color: #d61d47">
			<h3 class="text-white text-2xl md:text-5xl uppercase md:leading-normal">
				Cluster de Seguridad Industrial, Protección e Inteligencia de Baja California.
			</h3>
			<p class="mt-7 text-red-300 text-xs md:text-base font-bold">© {{ date('Y') }}, {{ env('APP_NAME') }}, Derechos Reservados.</p>
		</div>
	</div>
</x-guest-layout>